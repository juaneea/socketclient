import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;

public class ServerFeedback {
    private Socket TCPsocket;
    private PrintWriter output;
    private BufferedReader input;
    private DatagramSocket UDPsocket;
    private byte[] buffer;

    public void startTCPConnection(String host, int port) throws IOException {
        this.TCPsocket = new Socket(host, port);
        this.output = new PrintWriter(TCPsocket.getOutputStream(), true);
        this.input = new BufferedReader(new InputStreamReader(TCPsocket.getInputStream()));
        System.out.println("Connected to server: " + host + " " + port);
    }

    public void startUDPConnection(int port, int length) throws IOException {
        this.UDPsocket = new DatagramSocket(port);
        this.buffer = new byte[length];
    }

    public String sendMessage(String message) throws IOException {
        String response;
        System.out.println(message);
        this.output.println(message);
        response = this.input.readLine();
        if(response.equals("error invalidad user name") || response.equals("error invalid command")
                || response.equals("error unvalidated user") || response.equals("error invalid checksum format")) {
            System.out.println("Connection closed due error");
            this.TCPsocket.close();
        }
        System.out.println(response);
        return response;
    }

    public void destroyTCPConnection() throws IOException {
        this.input.close();
        this.output.close();
        this.TCPsocket.close();
        System.out.println("TCP Connection ended");
    }

    public String getUDPMessage() {
        String message = null;
        DatagramPacket packet = new DatagramPacket(this.buffer, this.buffer.length);
        try {
            this.UDPsocket.receive(packet);
            message = new String(packet.getData(), 0, packet.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(message);
        return message;
    }

    public void destroyUDPConnection() throws IOException {
        this.UDPsocket.close();
        System.out.println("UDP Connection ended");
    }
}
