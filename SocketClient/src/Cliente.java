import java.io.*;

public class Cliente {

    public static void main(String[] args) {
        ServerFeedback client = new ServerFeedback();
        ServerFeedback UDPListener = new ServerFeedback();

        try {
            client.startTCPConnection("localhost", 19876);

            client.sendMessage("helloiam jeescobar.17");
            int length = Integer.parseInt(client.sendMessage("msglen").split(" ")[1]);

            UDPListener.startUDPConnection(9000, length*3);
            client.sendMessage("givememsg 9000");
            String message = UDPListener.getUDPMessage();
            client.sendMessage("bye");

            client.destroyTCPConnection();
            UDPListener.destroyUDPConnection();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
